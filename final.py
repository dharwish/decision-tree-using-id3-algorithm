import numpy as np #numpy
import pandas as pd #pandas for dataframe
eps = np.finfo(float).eps #smallest number to avoid denominator 0 math error
from numpy import log2 as log #log to the base 2 as log
dataset = {'Soil':['Sandy','Loamy','Loamy','Loamy','Loamy','Clay','Sandy','Clay','Loamy','Sandy'],
       'Humidity':['High','High','High','Normal','High','Normal','Normal','High','Normal','High'],
       'Temperature':['Low','Low','High','High','High','Low','Low','Low','Low','High'],
       'Grows':['No','No','Yes','No','Yes','Yes','No','Yes','Yes','Yes']}
#datset
df = pd.DataFrame(dataset,columns=['Soil','Humidity','Temperature','Grows'])
entropy_node = 0  #Initialize Entropy
entropy_attribute = 0 #Initialize Entropy atribute
def find_entropy(df):
    Class = df.keys()[-1]   #To make the code generic, changing target variable class name
    entropy = 0
    values = df[Class].unique()
    for value in values:
        fraction = df[Class].value_counts()[value]/len(df[Class])
        entropy += -fraction*np.log2(fraction)
    return entropy
def find_entropy_attribute(df,attribute):
  Class = df.keys()[-1]   #To make the code generic, changing target variable class name
  target_variables = df[Class].unique()  #This gives all 'Yes' and 'No'
  variables = df[attribute].unique()    #This gives different features in that attribute (like 'Hot','Cold' in Temperature)
  entropy2 = 0
  for variable in variables:
      entropy = 0
      for target_variable in target_variables:
          num = len(df[attribute][df[attribute]==variable][df[Class] ==target_variable])
          den = len(df[attribute][df[attribute]==variable])
          fraction = num/(den+eps)
          entropy += -fraction*log(fraction+eps)
      fraction2 = den/len(df)
      entropy2 += -fraction2*entropy
  return abs(entropy2)
def find_winner(df):
    Entropy_att = []
    IG = []
    for key in df.keys()[:-1]:
        IG.append(find_entropy(df)-find_entropy_attribute(df,key))
    return df.keys()[:-1][np.argmax(IG)]
def get_subtable(df, node,value):
  return df[df[node] == value].reset_index(drop=True)
def buildTree(df,tree=None): 
    Class = df.keys()[-1]
    node = find_winner(df)   
    attValue = np.unique(df[node]) 
    if tree is None:                    
        tree={}
        tree[node] = {}
    for value in attValue:
        subtable = get_subtable(df,node,value)
        clValue,counts = np.unique(subtable['Grows'],return_counts=True)                        
        if len(counts)==1:#Checking purity of subset
            tree[node][value] = clValue[0]                                                    
        else:        
            tree[node][value] = buildTree(subtable) #Calling the function recursively                  
    return tree
tree = buildTree(df)
import pprint
pprint.pprint(tree)